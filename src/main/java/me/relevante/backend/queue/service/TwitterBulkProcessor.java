package me.relevante.backend.queue.service;

import me.relevante.api.NetworkCredentials;
import me.relevante.api.TwitterApi;
import me.relevante.api.TwitterApiException;
import me.relevante.api.TwitterApiProxy;
import me.relevante.api.TwitterApiResponse;
import me.relevante.api.TwitterApiResult;
import me.relevante.backend.queue.persistence.RelevanteAccountRepo;
import me.relevante.backend.queue.persistence.RelevanteContextRepo;
import me.relevante.backend.queue.persistence.TwitterActionDirectMessageRepo;
import me.relevante.backend.queue.persistence.TwitterActionFavRepo;
import me.relevante.backend.queue.persistence.TwitterActionFollowRepo;
import me.relevante.backend.queue.persistence.TwitterActionReplyRepo;
import me.relevante.backend.queue.persistence.TwitterActionRetweetRepo;
import me.relevante.backend.queue.persistence.TwitterFullUserRepo;
import me.relevante.backend.queue.requests.AccountRotationMarker;
import me.relevante.backend.queue.requests.BulkRequest;
import me.relevante.model.NetworkAction;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.model.TwitterActionDirectMessage;
import me.relevante.model.TwitterActionFav;
import me.relevante.model.TwitterActionFollow;
import me.relevante.model.TwitterActionReply;
import me.relevante.model.TwitterActionRetweet;
import me.relevante.model.TwitterFullUser;
import me.relevante.network.Twitter;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

@Service
public class TwitterBulkProcessor {

    private static final Logger logger = LoggerFactory.getLogger(TwitterBulkProcessor.class);
    private static final double MINIMUM_SECONDS_TO_WAIT = 5;
    private static final double MAXIMUM_SECONDS_TO_WAIT = 15;

    @Autowired protected TwitterFullUserRepo fullUserRepo;
    @Autowired protected TwitterActionDirectMessageRepo directMessageRepo;
    @Autowired protected TwitterActionFollowRepo followRepo;
    @Autowired protected TwitterActionFavRepo favRepo;
    @Autowired protected TwitterActionRetweetRepo retweetRepo;
    @Autowired protected TwitterActionReplyRepo replyRepo;
    @Autowired protected RelevanteAccountRepo relevanteAccountRepo;
    @Autowired protected RelevanteContextRepo relevanteContextRepo;

    private String lastProcessedRelevanteId;

    public TwitterBulkProcessor() {
    }

    public BulkRequest process(BulkRequest request) {

        // The account rotation marker is a fake request we put between different account requests so that
        // we mark the beginning of each account requests
        // This way we are able to process a request from a different account each time
        if (isAccountRotationMarker(request)) {
            this.lastProcessedRelevanteId = null;
            return null;
        }
        if (isRequiredAccountRotation(request)) {
            return request;
        }

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(request.getRelevanteId());
        RelevanteContext relevanteContext = relevanteContextRepo.findOne(request.getRelevanteId());

        if (!isAllowedTimeToProcess(relevanteContext)) {
            return request;
        }

        waitSomeRandomTime();
        NetworkAction action = getNetworkAction(request.getActionClass(), new ObjectId(request.getObjectId()));
        executeAction(action, relevanteAccount, relevanteContext);
        this.lastProcessedRelevanteId = relevanteAccount.getId();
        return new BulkRequest(request.getRelevanteId(), AccountRotationMarker.class.getSimpleName(), null);
    }

    private boolean isAccountRotationMarker(BulkRequest request) {
        return (request.getActionClass().equals(AccountRotationMarker.class.getSimpleName()));
    }

    private boolean isRequiredAccountRotation(BulkRequest request) {
        if (lastProcessedRelevanteId == null) {
            return false;
        }
        if (!lastProcessedRelevanteId.equals(request.getRelevanteId())) {
            return false;
        }
        return true;
    }

    private boolean isAllowedTimeToProcess(RelevanteContext relevanteContext) {
        if (!isDateSpacedEnoughFromLastAction(relevanteContext)) {
            return false;
        }
        if (!isDateInTimeRanges(LocalDateTime.now(), relevanteContext)) {
            return false;
        }
        return true;
    }

    private boolean isDateSpacedEnoughFromLastAction(RelevanteContext relevanteContext) {
        if (relevanteContext.getLastActionDate() == null) {
            return true;
        }
        LocalDateTime lastActionTime = LocalDateTime.ofInstant(relevanteContext.getLastActionDate().toInstant(), ZoneOffset.systemDefault());
        LocalDateTime nextAllowedTime = lastActionTime.plusSeconds(relevanteContext.getMinTimeBetweenActionsInSeconds());
        LocalDateTime now = LocalDateTime.now();
        if (now.isBefore(nextAllowedTime)) {
            return false;
        }
        return true;
    }

    private boolean isDateInTimeRanges(LocalDateTime date,
                                       RelevanteContext relevanteContext) {
        for (String timeRange : relevanteContext.getActionsTimeRanges()) {
            if (!isDateInTimeRange(date, timeRange)) {
                return false;
            }
        }
        return true;
    }

    private boolean isDateInTimeRange(LocalDateTime date,
                                      String timeRange) {
        String[] hoursInRange = timeRange.split("-");
        int startHour = Integer.parseInt(hoursInRange[0].split(":")[0]);
        int endHour = Integer.parseInt(hoursInRange[1].split(":")[0]);
        int dateHour = date.getHour();
        if (dateHour < startHour || dateHour > endHour) {
            return false;
        }
        return true;
    }

    private void waitSomeRandomTime() {
        long sleepTimeInSeconds = (long) Math.ceil(MINIMUM_SECONDS_TO_WAIT + (MAXIMUM_SECONDS_TO_WAIT - MINIMUM_SECONDS_TO_WAIT) * Math.random());
        long sleepTimeInMillis = sleepTimeInSeconds * 1000;
        try {
            Thread.sleep(sleepTimeInMillis);
        } catch (InterruptedException e) {
            logger.error("Error waiting some random time", e);
        }
    }

    private NetworkAction getNetworkAction(String actionClassName,
                                           ObjectId objectId) {

        NetworkAction networkAction = null;
        if (TwitterActionFav.class.getSimpleName().equals(actionClassName)) {
            networkAction = favRepo.findOne(objectId);
        } else if (TwitterActionRetweet.class.getSimpleName().equals(actionClassName)) {
            networkAction = retweetRepo.findOne(objectId);
        } else if (TwitterActionReply.class.getSimpleName().equals(actionClassName)) {
            networkAction = replyRepo.findOne(objectId);
        } else if (TwitterActionFollow.class.getSimpleName().equals(actionClassName)) {
            networkAction = followRepo.findOne(objectId);
        } else if (TwitterActionDirectMessage.class.getSimpleName().equals(actionClassName)) {
            networkAction = directMessageRepo.findOne(objectId);
        }
        return networkAction;
    }

    private void executeAction(NetworkAction action,
                               RelevanteAccount relevanteAccount,
                               RelevanteContext relevanteContext) {
        NetworkCredentials credentials = relevanteAccount.getCredentials(Twitter.getInstance());
        try {
            TwitterApi api = new TwitterApiProxy(credentials.getOAuthConsumerKeyPair(), credentials.getOAuthConsumerKeyPair(), credentials.getOAuthConsumerKeyPair(), credentials.getOAuthAccessTokenPair());
            Class clazz = action.getClass();
            if (clazz.equals(TwitterActionDirectMessage.class)) {
                TwitterActionDirectMessage directMessage = (TwitterActionDirectMessage) action;
                processDirectMessage(directMessage, api);
                directMessageRepo.save(directMessage);
            } else if (clazz.equals(TwitterActionFollow.class)) {
                TwitterActionFollow follow = (TwitterActionFollow) action;
                processFollow(follow, api);
                followRepo.save(follow);
            } else if (clazz.equals(TwitterActionFav.class)) {
                TwitterActionFav fav = (TwitterActionFav) action;
                processFav(fav, api);
                favRepo.save(fav);
            } else if (clazz.equals(TwitterActionRetweet.class)) {
                TwitterActionRetweet retweet = (TwitterActionRetweet) action;
                processRetweet(retweet, api);
                retweetRepo.save(retweet);
            } else if (clazz.equals(TwitterActionReply.class)) {
                TwitterActionReply reply = (TwitterActionReply) action;
                processReply((TwitterActionReply) action, api, credentials.getUserId());
                replyRepo.save(reply);
            }
        }
        catch (Exception e) {
            logger.error("Error executing bulk action ", e);
        }
        finally {
            relevanteContext.setLastActionDate(new Date());
            relevanteContextRepo.save(relevanteContext);
        }
    }

    private void processDirectMessage(TwitterActionDirectMessage directMessage,
                                      TwitterApi api) {
        TwitterApiResponse response;
        try {
            response = api.sendDirectMessage(Long.parseLong(directMessage.getTargetUserId()), directMessage.getMessage());
        }
        catch (TwitterApiException e) {
            response = new TwitterApiResponse(TwitterApiResult.FORBIDDEN, e);
        }
        processApiResult(directMessage, response.getResult());
    }

    private void processFollow(TwitterActionFollow follow,
                               TwitterApi api) {
        TwitterApiResponse response;
        try {
            response = api.follow(Long.parseLong(follow.getTargetUserId()));
        }
        catch (TwitterApiException e) {
            response = new TwitterApiResponse(TwitterApiResult.FORBIDDEN, e);
        }
        processApiResult(follow, response.getResult());
    }

    private void processFav(TwitterActionFav fav,
                            TwitterApi api) {
        TwitterApiResponse response;
        try {
            response = api.favTweet(Long.parseLong(fav.getPostId()));
        }
        catch (TwitterApiException e) {
            response = new TwitterApiResponse(TwitterApiResult.FORBIDDEN, e);
        }
        processApiResult(fav, response.getResult());
    }

    private void processRetweet(TwitterActionRetweet retweet,
                                TwitterApi api) {
        TwitterApiResponse response;
        try {
            response = api.retweet(Long.parseLong(retweet.getPostId()));
        }
        catch (TwitterApiException e) {
            response = new TwitterApiResponse(TwitterApiResult.FORBIDDEN, e);
        }
        processApiResult(retweet, response.getResult());
    }

    private void processReply(TwitterActionReply reply,
                              TwitterApi api,
                              String twitterUserId) {
        TwitterFullUser twitterFullUser = fullUserRepo.findOne(twitterUserId);
        TwitterApiResponse response;
        try {
            response = api.replyPost(Long.parseLong(reply.getPostId()), twitterFullUser.getProfile().getScreenName(), reply.getReplyText());
        }
        catch (TwitterApiException e) {
            response = new TwitterApiResponse(TwitterApiResult.FORBIDDEN, e);
        }
        processApiResult(reply, response.getResult());
    }

    private void processApiResult(NetworkAction networkAction,
                                  TwitterApiResult result) {
        if (result.equals(TwitterApiResult.SUCCESS)) {
            networkAction.setSuccess();
        }
        else if (result.equals(TwitterApiResult.ALREADY_PROCESSED)) {
            networkAction.setAlreadySucceeded();
        }
        else if (result.equals(TwitterApiResult.EXCEPTION)) {
            networkAction.setForbidden();
        }
        else {
            networkAction.setError();
        }
    }
}
