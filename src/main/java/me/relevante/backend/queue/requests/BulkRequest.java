package me.relevante.backend.queue.requests;

/**
 * Created by daniel-ibanez on 28/06/16.
 */
public class BulkRequest {

    private String relevanteId;
    private String actionClass;
    private String objectId;

    public BulkRequest() {
    }

    public BulkRequest(String relevanteId,
                       String actionClass,
                       String objectId) {
        this.relevanteId = relevanteId;
        this.actionClass = actionClass;
        this.objectId = objectId;
    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public String getActionClass() {
        return actionClass;
    }

    public String getObjectId() {
        return objectId;
    }

    @Override
    public String toString() {
        return "BulkRequest{" +
                "relevanteId='" + relevanteId + '\'' +
                ", actionClass='" + actionClass + '\'' +
                ", objectId='" + objectId + '\'' +
                '}';
    }
}
