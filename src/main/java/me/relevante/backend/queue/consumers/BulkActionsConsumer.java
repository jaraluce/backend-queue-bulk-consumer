
package me.relevante.backend.queue.consumers;

import com.fasterxml.jackson.core.JsonProcessingException;
import me.relevante.backend.queue.QueueName;
import me.relevante.backend.queue.consumer.BaseConsumer;
import me.relevante.backend.queue.persistence.RelevanteAccountRepo;
import me.relevante.backend.queue.persistence.RelevanteContextRepo;
import me.relevante.backend.queue.requests.BulkRequest;
import me.relevante.backend.queue.service.TwitterBulkProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class BulkActionsConsumer extends BaseConsumer {

    private static final Logger logger = LoggerFactory.getLogger(BulkActionsConsumer.class);

    @Autowired protected RelevanteAccountRepo relevanteAccountRepo;
    @Autowired protected RelevanteContextRepo relevanteContextRepo;
    @Autowired protected TwitterBulkProcessor twitterBulkProcessor;

    @RabbitListener(queues = QueueName.BULK_ACTIONS)
	public void processTwitterBulkActions(byte[] data) {

        BulkRequest request = getBulkRequest(data);

        try {
            BulkRequest generatedRequest = twitterBulkProcessor.process(request);
            if (generatedRequest != null) {
                Message message = createRequestMessage(generatedRequest);
                rabbitTemplate.send(QueueName.BULK_ACTIONS, message);
            }
        }
        catch (Exception e) {
            logger.error("Error processing Twitter bulk action", e);
        }
    }

    private BulkRequest getBulkRequest(byte[] data) {
        try {
            BulkRequest queueRequest = objectMapper.readValue(data, BulkRequest.class);
            return queueRequest;
        }
        catch (IOException e) {
            throw new IllegalArgumentException("Queue Request data is not valid");
        }
    }

    private Message createRequestMessage(BulkRequest request) {
        try {
            byte[] data = objectMapper.writeValueAsBytes(request);
            Message message = MessageBuilder.withBody(data).build();
            return message;
        }
        catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
    }

}