package me.relevante.backend.queue.persistence;

import me.relevante.model.TwitterActionDirectMessage;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterActionDirectMessageRepo extends MongoRepository<TwitterActionDirectMessage, ObjectId> {
    TwitterActionDirectMessage save(TwitterActionDirectMessage directMessage);
}