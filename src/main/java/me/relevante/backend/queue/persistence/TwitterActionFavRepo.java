package me.relevante.backend.queue.persistence;

import me.relevante.model.TwitterActionFav;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterActionFavRepo extends MongoRepository<TwitterActionFav, ObjectId> {
    TwitterActionFav save(TwitterActionFav fav);
}