package me.relevante.backend.queue.persistence;

import me.relevante.model.TwitterActionFollow;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterActionFollowRepo extends MongoRepository<TwitterActionFollow, ObjectId> {
    TwitterActionFollow save(TwitterActionFollow follow);
}