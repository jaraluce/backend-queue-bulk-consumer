package me.relevante.backend.queue.persistence;

import me.relevante.model.TwitterActionReply;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterActionReplyRepo extends MongoRepository<TwitterActionReply, ObjectId> {
    TwitterActionReply save(TwitterActionReply reply);
}