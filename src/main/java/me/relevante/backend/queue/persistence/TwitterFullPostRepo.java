package me.relevante.backend.queue.persistence;

import me.relevante.model.TwitterFullPost;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterFullPostRepo extends MongoRepository<TwitterFullPost, String> {
    TwitterFullPost save(TwitterFullPost linkedinUser);
}