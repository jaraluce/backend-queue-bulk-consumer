package me.relevante.backend.queue.persistence;

import me.relevante.model.TwitterActionRetweet;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterActionRetweetRepo extends MongoRepository<TwitterActionRetweet, ObjectId> {
    TwitterActionRetweet save(TwitterActionRetweet retweet);
}